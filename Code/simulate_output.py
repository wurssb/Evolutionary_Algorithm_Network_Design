# -*- coding: utf-8 -*-
"""
Created on 13 March 2017

@author: R W Smith & B van Sluijs
"""

# the output file is required to process the data or obtained networks that is stored in textfiles on your computer after a sucessfull EA run
# it uses parts of the algorithm to recreate the concentration profiles by recreating the model
# this also allows the user to perform further analysis on the mode
# because all relevant data is stored (parameters, abstract network, model network)
# the user can perfrom different analyses such as parameter perturbations, stochastic simulations, parameter sensitivities etc
#the first part of the script creates the network and takes the "order" dictionary so the model can be recreated the second part performs the analysis
#again this is up to the user and whom can write his or her own functions or use those that are part of the algorithm

import numpy
import matplotlib.pylab as plt
import copy
import os
from numpy import array 
import sympy

from Graph import *
from Build import *
from Reactions import *
from Solve import *

#the dict reduction function is used to flatten the parameter dictionary for parameter pertubation studies
def dict_reduction(x):
    y = {}
    for i in range(len(x)):
        y.update(x[i])
    return y

output = []
path_network = '.txt'
with open(path_network, 'r') as network:
    data_network    = network.read()
data_network = data_network.replace('\n','')
data_network = data_network.replace('}}}{','}}}\n{')
data_network = data_network.replace('nan','0')
data_network = data_network.replace('       ','')
data_network = data_network.replace('\xe2\x80\x98',"'")
with open('.txt','w') as outfile:
    outfile.write(data_network)
path =  '.txt'
with open(path, 'r') as network:
    for line in network.readlines():
        output.append(eval(line))

# this loop takes the text file with the networks (adjacency matrx, binary genes, parameters and order dictionary)
#it removes all redundant text and reevaluates all the network parts (as they were in the EA)
#output contains the networks that were obtained in the evolution of the system

keys                            = output[0]['generation_select'][0].keys()
master_keys                     = output[0].keys()
# set this according to the length of thebinary gene subsections, if this is wrong you will end up with the wrong model
simulation_time                 = 240  #set simulation time in MINUTES!
Promoter_gate                   = 3    # length of binary gene subuntis as shown in pool (dont make to long, number of allocated substrings increases factorially)
Promoter_interaction_type       = 3
Gene_product                    = 3
Promoter_expression_type        = 3
Gene_product_subunits           = 3
Gene_product_connections        = 3
operon                          = Promoter_gate + Promoter_interaction_type + Gene_product + Promoter_expression_type + Gene_product_subunits + Gene_product_connections
plasmid_number = 10
#notice how this is the same as the setup as the main file

oscillation_score               = []

 #the folowing section of the plots the concentration profiles of the obtained networks in the EA
 #and calculates the eigenvalues. the models are run for a long time in order to ensure steady state behavior
for i in range(len(output)):
    a_r     = output[i]
    order   = a_r['order']
    pool    = a_r['pool']
    oscillation_score.append(a_r['score'])
    network = a_r['generation_select'][0]['network']
    values  = a_r['generation_select'][0]['param_values']
    #values  = dict_reduction(values)
 ### these 3 functions are taken directly from the algorithm (the graph, build and sovle functions)
    interaction_graph,intrinsic,basal = create_graph(network,order,Promoter_gate,Promoter_interaction_type,Gene_product,Promoter_expression_type , Gene_product_subunits,Gene_product_connections,pool)
    model_components = reaction_equation(network,interaction_graph,pool,intrinsic,{},[])
    equations,IC,new_state,M_matrix,stochvector,System = matrix_construction(model_components,values,plasmid_number)
    result = solver_odeint(equations,IC,simulation_time)
    if i == len(output)-1:
        plt.plot(result)
        plt.show()
    