# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 16:04:52 2016

@author: bobvdsluijs
"""
from __future__ import division
import numpy
import random
import scipy
from scipy import integrate
from scipy.interpolate import splprep, splev
from collections import defaultdict
from operator import add
import matplotlib.pylab as plt
from math import factorial


# after the models are created they need to be solved, this can be done in a stochastic and deterministic fashion
# the stochastic fashion takes the M matrix and the fluxvector and creates a gillespie algorithm with this information and solves these systems accodingly
# the deterministic function builds the models with the equations and initial states and implements this model in the odeint solver, see python cookbook for an example odeint ode solving

def model_build(S_system):
    string = ';'.join('dydt[{}] = {}'.format(i, S_system[i]) for i in range(len(S_system)))
    string = compile(string, 'state_function', 'exec') # you have to precompile the equation system, if you do not it will have to execute the string everytime it makes a calulation. making is at least 100* slower
    def model(y,t):
        dydt = numpy.zeros(len(S_system))
        exec string in locals()
        return dydt
    return model # you return a model function that can be inputted into the odeint solver 
    
#odeint solver which intergrates the system with integrate,odeint
def solver_odeint(S_system,IC,simulation_time):
    model = model_build(S_system)
    y = integrate.odeint(model,IC,numpy.linspace(0,simulation_time,60*simulation_time)) # I take 60 time steps per minute, so every iteration delta t in the numerical integrator is a second 
    return y
        
        
 ########### ########### ########### ############  
# the previous model builder does not take into account inputs into the system
# by taking inputs into account means we divide the integrator up in time intervals
        # the function checks wether an input has to occur in a time interaval
            # if no:
                # the input rate of the inducer (wheter direct or indirect) = 0
                # the next time interval is solved with the final concentrations of the previous time interval as the initial conditions of the next time interval
            # if yes:
                # the input rate of the inducer (wheter direct or indirect) = user defined rate
                # the next time interval is solved with the final concentrations of the previous time interval as the initial conditions of the next time interval

# the only difference between model_build_base and model_build_input is the value of the input rate parameter/s            
def model_build_base(S_system,input_parameters):
    string = ';'.join('dydt[{}] = {}'.format(i, S_system[i]) for i in range(len(S_system)))
    for i in input_parameters:
        string = string.replace(i,'0')
    string = string.replace('+    *', ' + ') # patch for a bug
    string = compile(string, 'state_function', 'exec')
    def model(y,t):
        dydt = numpy.zeros(len(S_system))
        exec string in locals()
        return dydt
    return model 
    
def model_build_input(S_system,input_parameters,parameters):
    string = ';'.join('dydt[{}] = {}'.format(i, S_system[i]) for i in range(len(S_system)))
    for i in parameters:
        string = string.replace(i,str(parameters[i]))
    for i in input_parameters:
        string = string.replace(i,'0') 
    string = string.replace('+    *', ' + ')
    string = compile(string, 'state_function', 'exec')
    def model(y,t):
        dydt = numpy.zeros(len(S_system))
        exec string in locals()
        return dydt
    return model 
    
# this odeint solver is for the input and requires a bit of work. 
# the input pattern given in main is 'translated' into time intervals where certain inputs are given
def solver_odeint_input(S_system,IC,simulation_time,input_map,model_components,input_pattern,new_state,pulse_size,input_type,output):
    protein_ID    = [model_components['current_ID'][i] for i in output]
    index_data    = [new_state[i] for i in protein_ID]
    index         = []
    new_y         = numpy.zeros((simulation_time*60,len(IC))) 
    hour          = numpy.linspace(0,1,60) #set time interval
    input_range      = range(len(input_map))
    input_parameters = [i[1] for i in  model_components['reaction_map']]
    parameters    = {}
    if input_type == 'indirect': # find out which parameter belongs to which input
        for k in input_parameters:
            for i in k:
                if i.isdigit():
                    parameters.update({eval(i):{input_parameters[eval(i)]:pulse_size}})  
    elif input_type == 'direct':
        for k in input_parameters:
            for i in k[0:3]:
                if i.isdigit():
                    parameters.update({eval(i):{input_parameters[eval(i)]:pulse_size}})
                    
    for i in index_data:
        i = i.strip('y')
        i = i.strip(']')
        i = i.strip('[')
        index.append(eval(i))   
    count = 0                     
    for i in input_pattern: # find which input is given at which time 
        if i == None:
            model = model_build_base(S_system,input_parameters)
            y  = integrate.odeint(model,IC,hour)
            IC = [n for n in y[-1]]
            for j in range(len(IC)):
                new_y[:,j][60*count:60*(count+1)] = y[:,j]
        if i in input_range: # if input  in the range f possible onputs build model with that input rate altered and solve 
            model = model_build_input(S_system,input_parameters,parameters[i])
            y = integrate.odeint(model,IC,hour)
            IC= [n for n in y[-1]]
            for j in range(len(IC)):
                new_y[:,j][60*count:60*(count+1)] = y[:,j]
        count += 1
    return new_y
    
    
#the gillespie solver does not take inputs it simply takes the stoichiometric matrix the flux vector and initial conditions and solves them stochastically         
def solver_gillespie(state,reactions,stoichiometry,output,new_state,model_components):
    product_ID    = model_components['current_ID']
    def create_model(state, reactions, stoichiometry):   # place the matrices in gillespie format, create a back track map so that not all propensities have to be updated only the altered ones
                                                         # assign column and row integers to reaction rates from which propensities are calculated such that the IC can be updated each reactions
                                                         # by adding a row of +1s, -1s ans 0s to the current state
        S = numpy.array(stoichiometry)
        state = numpy.array(state)
        flux_constants = numpy.zeros(len(reactions))
        flux_dict = defaultdict(list)
        rev_flux_dict = defaultdict(list)
        for i, reaction in enumerate(reactions):
            for term in reaction.split('*'):
                term = term.strip()
                try:
                    flux_constants[i] = float(term)
                except ValueError:
                    flux_dict[i].append(int(term.strip('y[]')))
    
        for key, values in flux_dict.items():
            for value in values:
                rev_flux_dict[value].append(key)
        return S, state, flux_constants, flux_dict, rev_flux_dict
    
    # @profile
    def compute_propensities(state, flux_constants, flux_dict): # the propensities of the updated systems are calculates 
        propensities = numpy.array(flux_constants)
        for i, states in flux_dict.items():
            for s in states:
                propensities[i] *= state[s]
        return propensities
    
    # @profile
    def solve(state, reactions, stoichiometry, max_t):
        S, state, flux_constants, flux_dict, rev_flux_dict = create_model(state, reactions,
                                                                          stoichiometry)
        t = 0
        state_history = [numpy.array(state)]
        time_history = [t]
    
        p = numpy.array(flux_constants)
        for i, states in flux_dict.items():
            for s in states:
                p[i] *= state[s]
    
        i = -1
        while t < max_t: # create a list of intervals corresponding to a reaction
            p_cumsum = numpy.cumsum(p)
            p_sum = p_cumsum[-1]
            p_cumsum /= p_sum
    
            # Pick reaction
            rand = random.random() # pick a reaction by drawing a random number that falls in a interval with said reaction
            r = numpy.searchsorted(p_cumsum, rand)
    
            # Update
            dstate = S[:, r]   # update the state
            state += dstate
            # Update propensities which are altered, don't do unessecary calculations.
            for si in numpy.where(dstate)[0]:
                for pi in rev_flux_dict[si]:
                    p[pi] = flux_constants[pi]
                    for s in flux_dict[pi]:
                        p[pi] *= state[s]
    
            t += 1.0 / p_sum * numpy.log(1.0 / rand) # update the time and create time
    
            state_history.append(numpy.array(state)) # create two arrays with the time course data of your simulation (concentration)
            time_history.append(t)                   # (time)                  
        return  numpy.array(time_history), numpy.array(state_history)
    
    #use a savitzky golay to filter the gillespy results. there are many reactions that occur on a immesurable timescale, only their trend is mesured
    #we can therefore filter the result to accomodate this and to end up with a less noisy signal
    def savitzky_golay(y, window_size, order, deriv=0, rate=1):   
        try:
            window_size = numpy.abs(numpy.int(window_size))
            order = numpy.abs(numpy.int(order))
        except ValueError, msg:
            raise ValueError("window_size and order have to be of type int")
        if window_size % 2 != 1 or window_size < 1:
            raise TypeError("window_size size must be a positive odd number")
        if window_size < order + 2:
            raise TypeError("window_size is too small for the polynomials order")
        order_range = range(order+1)
        half_window = (window_size -1) // 2
        # precompute coefficients
        b = numpy.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
        m = numpy.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
        # pad the signal at the extremes with
        # values taken from the signal itself
        firstvals = y[0] - numpy.abs( y[1:half_window+1][::-1] - y[0] )
        lastvals = y[-1] + numpy.abs(y[-half_window-1:-1][::-1] - y[-1])
        y = numpy.concatenate((firstvals, y, lastvals))
        return numpy.convolve( m[::-1], y, mode='valid')    

    def analyse_gillespie(data,variance_data,stoch_sim_time,number_of_states,gillespie_total):
        mean_data = [data[i]/gillespie_total for i in range(number_of_states)]
        for i in variance_data:
            count = 0
            total = 0
            for j in i:
                total += numpy.sum((mean_data[count]-j)**2)
                count += 1
        variance = float(total)/(number_of_states*gillespie_total)
        return  mean_data,variance

    # This part of the stochastic solver initializes the gillespie.
    # it is here that you state how long you want the gillespie to run in MINUTES. very long simulation times for large systems (concentration and high reaction rates)
    # will result in a very slow convergence to the final time!!!! be carefull
    # consider that on an average computer this solver can calculate roughly 30.000 reactions per second
        
    data           = []
    variance_data  = []
    stoch_sim_time = 1000 # set time in minutes
    time_vector = numpy.arange(0.0, stoch_sim_time, 1.0 / 60.0)
    gillespie_total = 1 #set the number of stochastic runs for every model that is solved. remember it is a single realization of the master equation and therefore not unique
    for i in range(gillespie_total):
        t, s = solve(state, reactions, stoichiometry, stoch_sim_time)
        number_of_states = s.shape[1]
        variance = []
        if i == 0:
            for j in range(number_of_states):
                interpolated_data = numpy.interp(time_vector, t, s[:, j])
                data.append(interpolated_data)
                variance.append(interpolated_data)
        else:
            for j in range(number_of_states):
                interpolated_data = numpy.interp(time_vector, t, s[:, j])
                data[j] += interpolated_data
                variance.append(interpolated_data)
            variance_data.append(variance)
        
#    mean,variance = analyse_gillespie(data,variance_data,stoch_sim_time,number_of_states,gillespie_total)  # if the user so desired, calculate the variance with more gillespie runs
    #interpolate the mean of the data
    sg_mean = [savitzky_golay(i,window_size = 277, order = 2) for i in data]
    for i in range(len(sg_mean)):
        plt.plot(sg_mean[i])
    plt.show()
    return sg_mean,variance
        
def scoring_data_gillespie(y,objective,variance):
    sum_of_squares = 0 
    score = 0
    objective = list(objective)
    for i in range(len(variance)): #or y       
        sum_of_squares += numpy.log(sum((y[i] - objective[i])**2))
        score += sum_of_squares*variance[i]
    return score,y
#########################################################################################################   

