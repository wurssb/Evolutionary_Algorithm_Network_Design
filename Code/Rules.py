# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 12:46:30 2016

@author: bobvdsluijs
"""

import random
import itertools
import numpy


#This function  sitributesthe stringsubunits according to the user defined distribution found in pool main. it distributes them according to how many permutations
#are available accoring to the length of the subuints also found in main.

#This resutls in a a dictionary called 'order', which serves as a key that can translate the binary strings into something meaningfull e.g. a graph that in turn can be
#translated into a set of reaction equations. 

def set_rules(Promoter_gate,Promoter_interaction_type,Gene_product,Promoter_expression_type,Gene_product_subunits,Gene_product_connections,pool):
    select = lambda n: random.choice([x for x in n for y in range(n[x])])    
    Promoter_gate_combination             = list(itertools.product((0,1), repeat = Promoter_gate))             #create all permutions of binary string. for length 3 this becomes
    Promoter_interaction_combination      = list(itertools.product((0,1), repeat = Promoter_interaction_type)) #(000,001,010,100,110,101,011,111)
    Gene_product_combination              = list(itertools.product((0,1), repeat = Gene_product))
    Promoter_expression_combination       = list(itertools.product((0,1), repeat = Promoter_expression_type))
    Gene_product_subunits_combination     = list(itertools.product((0,1), repeat = Gene_product_subunits))   
    Gene_product_connections_combination  = list(itertools.product((0,1), repeat = Gene_product_connections))   
    
    random.shuffle(Promoter_gate_combination)#shuffle the list of binary permutions          
    random.shuffle(Promoter_interaction_combination)
    random.shuffle(Gene_product_combination)
    random.shuffle(Promoter_expression_combination)
    random.shuffle(Gene_product_subunits_combination)
    random.shuffle(Gene_product_connections_combination)    
              
    pg = {}; pi = {}; pe = {}; gp = {}; gps = {}; gpc = {}; #according to pool
    for i in Promoter_gate_combination: # iterate through list and assign to pool class
        pg.update({i:(select(pool['Promoter_gate']))}) #create a dictionary of dictionaries where {poolsubstring: {Binary string i: substring attribute e.g AND Gate selected at random}}
        
    for i in Promoter_interaction_combination:
        pi.update({i:(select(pool['Promoter_interaction_type']))})  
        
    for i in Promoter_expression_combination:
        pe.update({i:(select(pool['Promoter_expression_type']))})
        
    for i in Gene_product_combination:
        gp.update({i:(select(pool['Gene_product']))})
        
    for i in Gene_product_subunits_combination:
        gps.update({i:(select(pool['Gene_product_subunits']))}) 
        
    for i in Gene_product_connections_combination:
        gpc.update({i:(select(pool['Gene_product_connections']))})
    order = {'Promoter_gate':pg,'Promoter_interaction_type':pi,'Promoter_expression_type':pe,'Gene_product':gp,'Gene_product_subunits':gps,'Gene_product_connections':gpc}
    # {promoter_gate: {(0,1,0):AND,(1,1,1): AND, (0,0,1): OR} ...........etc etc}
    return order