###
# This is a test code to find the key points in the evolutionary algorithm where the score changes greatest
# i.e. one can plot an evolutionary trajectory of the score and obtain the networks at each point where the objective score decreases
# depending on the problem this code will need to be edited due to what reactions are allowed in the algorithm.
#
# Author: R W Smith & B van Sluijs

import sys
# append path to where the main EA code is located
sys.path.append('')
import numpy
import matplotlib.pylab as plt
import copy
import os
from numpy import array 
import sympy
from scipy.fftpack import rfft
from collections import Counter
from scipy.fftpack import fft
from Graph import *
from Build import *
from Reactions import *
from Solve import *
from Score import *
import json

def dict_reduction(x):
    y = {}
    for i in range(len(x)):
        y.update(x[i])
    return y
    
def perturbe_values(values):
    value_keys       = values.keys() 
    parameter_number = len(value_keys)
    pertubation      = [random.random() for i in range(parameter_number)]
    count = 0
    for i in value_keys:
        plus_minus = random.random()
        if plus_minus > 0.5:
            values[i] = values[i] - (values[i]*pertubation[count])
        else:
            values[i] = values[i] + (values[i]*pertubation[count])            
        count += 1
    return values, pertubation

maxG = #maximum number of EA runs to be analysed
for number in range(0,maxG,1):
    EA_output = []
    with open('.txt'.format(number), 'r') as network:
        data_network    = network.read()
    data_network = data_network.replace('\n','')
    data_network = data_network.replace('}}}{','}}}\n{')
    data_network = data_network.replace('nan','0')
    data_network = data_network.replace('       ','')
    data_network = data_network.replace('\xe2\x80\x98',"'")
    with open('./temp.txt','w') as outfile:
        outfile.write(data_network)
    path =  './temp.txt'
    with open(path, 'r') as network:
        output = []
        for line in network.readlines():
            output.append(eval(line))
    EA_output.append(output)

    increment = {1./float(i+1):float(i)/100. for i in range(100)}
    for k in EA_output:
        score = []
        for j in k:
            N = j['generation_select'][0]['score']['objective_score'][0]
            score.append(N)
    # Plots evolutionary trajectory
    plt.figure(number)
    plt.plot(score)
    plt.title("Evolving Parameter Robustness",fontsize = 12)
    plt.ylabel("Fraction",fontsize = 10)
    plt.xlabel("Generation", fontsize = 10)
    plt.savefig('.eps'.format(number),dpi = 600)
    plt.show(block=False)

    new = [str(output[i]) for i in range(len(output))]
    variance_dict = {i:'' for i in range(len(output))}
    stoch_dict    = {i:'' for i in range(len(output))}


    final_networks = [i[-1] for i in EA_output]
    peaks = copy.deepcopy(EA_output)

    # These need to be edited such that they are the same values as in main.py
    Promoter_gate                   = 3
    Promoter_interaction_type       = 5
    Gene_product                    = 5
    Promoter_expression_type        = 3
    Gene_product_subunits           = 3
    Gene_product_connections        = 3
##############################################################################
    iteration = 0
    news = [str(i['generation_select'][0]) for i in EA_output[0]]
    print len(news)
    news = list(set(news))
    print len(news)
    news = [eval(i) for i in news]
    print news[0]

    order = EA_output[0][0]['order']
    pool = EA_output[0][0]['pool']

    network_structure = {}
    gene_structure = {}
    parameters = {}
    scores = {}
    network_structure = {i:[] for i in range(len(news))}
    gene_structure = {i:[] for i in range(len(news))}
    parameters   = {i:[] for i in range(len(news))}
    scores   = {i:[] for i in range(len(news))}

    for i in range(len(news)):
        news[i].update({'pool':pool})
        news[i].update({'order':order})

    for i in news:
        adjacency_matrix   = i['network']['adjacency_matrix']
        network            = i['network']
        order              = i['order']
        pool               = i['pool']
        value              = i['param_values']
        score              = i['score']['objective_score'][0]
        interaction_vector = numpy.nonzero(adjacency_matrix)
        parameters[iteration] = value
        scores[iteration] = score
        edgelist   = numpy.vstack(interaction_vector)
        edgelist   = [tuple(edgelist[:,i]) for i in range(len(edgelist[0]))]
        label_list = {};gate = {}; interaction_type = {}; expression_type = {};product = {}; subunits  = {}; connections  = {};
        symbolic_adjacency_matrix = copy.deepcopy(adjacency_matrix)
        symbolic_adjacency_matrix = symbolic_adjacency_matrix.astype(dtype = 'S')

        for j in range(len(network['genes'])):

            # This part of the code needs to be edited to match the options selected in main.py as to feasible reactions
            pg = tuple(network['genes'][j][0:Promoter_gate])
            pi = tuple(network['genes'][j][Promoter_gate:Promoter_gate+Promoter_interaction_type])
            pe = tuple(network['genes'][j][Promoter_gate+Promoter_interaction_type:Promoter_gate+Promoter_interaction_type+Promoter_expression_type])
            gp = tuple(network['genes'][j][Promoter_gate+Promoter_interaction_type+Promoter_expression_type:Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product])
            gs = tuple(network['genes'][j][Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product:Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product+Gene_product_subunits])
            gc = tuple(network['genes'][j][Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product+Gene_product_subunits:Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product+Gene_product_subunits+Gene_product_connections])

            a=order['Promoter_gate'][pg]
            b=order['Promoter_interaction_type'][pi]
            c=order['Promoter_expression_type'][pe]
            d=order['Gene_product'][gp]
            e=eval(order['Gene_product_subunits'][gs][1])
            f=eval(order['Gene_product_connections'][gc][1])
       
            gate.update({j:a})
            interaction_type.update({j:b})
            expression_type.update({j:c})
            product.update({j:d})
            subunits.update({j:e})
            connections.update({j:f})
    
            network_structure[iteration] = symbolic_adjacency_matrix
            gene_structure[iteration].append(a)
            gene_structure[iteration].append(c)
            gene_structure[iteration].append(e)
            gene_structure[iteration].append(f)
        iteration += 1
        # This part of the code needs to be edited to match the options selected in main.py as to feasible reactions
        indices = numpy.nonzero(network['adjacency_matrix'])
        interaction_vector = numpy.vstack(indices)
        interactions       = ['kP_A','kP_R','kG_A','kG_R']
        graph_structure  = {}
        for i in interactions:
            graph_structure.update({i:[]})
        count = 0
        for k in range(len(indices[0])):
            if product[indices[0][count]]   == 'PR_A':
                graph_structure['kP_A'].append(interaction_vector[:,count])
                
            elif product[indices[0][count]] == 'PR_R':                   
                graph_structure['kP_R'].append(interaction_vector[:,count])
                         
            elif product[indices[0][count]] == 'AR' and interaction_type[indices[1][count]] == 'AR':    
                graph_structure['kG_A'].append(interaction_vector[:,count])                 
    
            elif product[indices[0][count]] == 'RA' and interaction_type[indices[1][count]] == 'RA':    
                graph_structure['kG_A'].append(interaction_vector[:,count])  
            
            elif product[indices[0][count]] == 'AR' and interaction_type[indices[1][count]] == 'RA':    
                graph_structure['kG_R'].append(interaction_vector[:,count]) 
                
            elif product[indices[0][count]] == 'RA' and interaction_type[indices[1][count]] == 'AR':    
                graph_structure['kG_R'].append(interaction_vector[:,count])  
            count += 1 

        for n in interactions:
            for j in graph_structure[n]:
                symbolic_adjacency_matrix[j[0],j[1]] = n

    network_rep = [str(i) for i in network_structure.values()]
    n_rep       = len(set(network_rep))
    gene_rep    = [str(i) for i in gene_structure.values()]
    g_rep       = len(set(gene_rep))
    
    # Saves all key information about important networks in text files (adjacency matrices, gene strings, parameter values and score)
    with open('.txt'.format(number),'w') as outfile:
            for j in range(0,len(parameters),1):
                outfile.write("Network "+str(j))
                outfile.write("\n")
                outfile.write(str(parameters[j]))
                outfile.write("\n")
    outfile.close()
    with open('.txt'.format(number),'w') as outfile:
        count = 0
        for i in range(0,len(scores),1):
            outfile.write("Network "+str(count))
            outfile.write("\n")
            outfile.write(str(scores[i]))
            outfile.write("\n")
            count+=1
    outfile.close()
    with open('.txt'.format(number),'w') as outfile:
        count = 0
        for i in network_rep:
                outfile.write("Network "+str(count))
                outfile.write("\n")
                outfile.write(i)
                outfile.write("\n")
                count+=1
    outfile.close()
    with open('.txt'.format(number),'w') as outfile:
        count = 0
        for i in gene_rep:
            outfile.write("Gene/Node string in Network "+str(count))
            outfile.write("\n")
            outfile.write(i)
            outfile.write("\n")
            count+=1
    outfile.close()