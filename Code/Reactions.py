# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 12:46:31 2016

@author: bobvdsluijs
"""


import copy

#this script trnaslates the graph into actual reactio equations according to [[reactants],rate,[product]]
#you can rewrite this as you please. As long as the format of [[], []] holds and genes are annotated with g1 g2 g3 g4 etc the rest of the algorithm still works.
#it is not the most consice function (understatement cough cough *), newer version will streamline it into classes

    #set of functions that creates the [[], []] lists. this occurs sequentially. the current ID is updated each time an operation is performed.
    #say you have a protein g1mp that binds to another protein produced by another gene. after that function is called it becomes protein g1mpg2mp.
    # because the product of gene 2 operated on the product of gene 1 this complex remains the product of gene 1! this means that in the matrix
    # this compelx performs other operation as described by gene 1 and its interactions.
    
    # here you can rearance the way the reactions take place. if the reaction involces a current_ID the order matters for the structure of your final network
    # see appendix for more information on this.

##########################  BASE REACTIONS  ################################
def reaction_equation(network,interaction_graph,pool,intrinsic,input_map,input_genes):
    reaction       = []  #list where all reaction equations will be stored 
    current_ID     = dict(enumerate(network['genes'])) #this keeps track of the current ID (see appendix)
    gate = intrinsic['Promoter_gate']
    component_dict = {'protein':[],'mRNA':[],'genes':[]} # there are 3 different component types (usefull fur applying degredation rates)
    
    parameter_dict = {'Transcribe':[],'Translate':[],
    'ProteinBinding':[],   'ProteinUnbinding':[],    
    'ProteinActivation':[],'ProteinDeactivation':[],
    'GeneActivation':[],'Transport':[],
    'ProteinDegredation': [],'mRNADegredation':[],'Catalysis':[]} 
    input_parameter_dict = {'InputActivation':[],'InputDeactivation':[],'InputPassive':[]}
               
               
    #set of functions that creates the [[], []] lists. this occurs sequentially. the current ID is updated each time an operation is performed.
    #say you have a protein g1mp that binds to another protein produced by another gene. after that function is called it becomes protein g1mpg2mp.
    # because the product of gene 2 operated on the product of gene 1 this complex remains the product of gene 1! this means that in the matrix
    # this complex performs other operation as described by gene 1 and its interactions.

        
    def base_genes(network,intrinsic):
            return ['g'+ str(i) for i in range(len(network['genes']))]
    def base_mRNA(network,intrinis,precursor): 
            return [precursor[i] + 'm' + str(j) for i in range(len(network['genes'])) for j in range(intrinsic['Gene_product_subunits'][i])]
    def base_components(network,intrinisc,precursor,current_ID):
            base_components = [];count = 0
            for i in range(len(network['genes'])):
                for j in range(intrinsic['Gene_product_subunits'][i]):
                    base_components.append(precursor[count] + intrinsic['Gene_product'][i])
                    if j <= 1:
                        current_ID
                        current_ID[i] = precursor[count] + intrinsic['Gene_product'][i]
                    count += 1  
            return base_components,current_ID
   
    def mRNA_graph(network,intrinsic):
        mRNA_graph = {}
        for i in range(len(network['genes'])):
            mRNA_graph.update({i:[]})
            for j in range(intrinsic['Gene_product_subunits'][i]):
                mRNA_graph[i].append('g'+ str(i) + 'm'+ str(j))
        return mRNA_graph
   
    def irreversible(parameter_dict,parameter_type,reactants,rate,product):
        parameter_dict[parameter_type].append(''.join(reactants)+rate)
        return [reactants,''.join(reactants)+rate,product]
    def irreversible_conserved(parameter_dict,parameter_type,reactants,rate,product,conserved_components):
        parameter_dict[parameter_type].append(''.join(reactants)+rate)
        return [reactants,''.join(reactants)+rate,product+conserved_components]
        
    def transcribe(parameter_dict,reaction,genes,intrinsic):
        for i in range(len(genes)):
            product = [genes[i]+'m'+str(j) for j in range(intrinsic['Gene_product_subunits'][i])]
            reaction.append(irreversible_conserved(parameter_dict,'Transcribe',[genes[i]],'k_trcb',product,[genes[i]]))
        return 

    def transport(parameter_dict,component_dict,component_type,reaction,reactants):
        for i in reactants:
            component_dict[component_type].append([i+'t'])
            reaction.append(irreversible(parameter_dict,'Transport',[i],'k_trsp',[i+'t']))
        return 

    def translate(parameter_dict,reaction,mRNA,proteins,current_ID):
        for i in range(len(mRNA)):
            reaction.append(irreversible_conserved(parameter_dict,'Translate',[mRNA[i]],'k_trns',[proteins[i]],[mRNA[i]]))
        return 

    def protein_formation(parameter_dict,component_dict,reaction,genes,proteins,intrinsic,current_ID):
        count = 0        
        for i in range(len(genes)):
            product = ''
            count_protein = 0
            for j in range(intrinsic['Gene_product_subunits'][i]-1):
                if count_protein == 0:
                    product = proteins[count] + proteins[count+1]
                    reaction.append(irreversible(parameter_dict,'ProteinBinding',[proteins[count],proteins[count+1]],'k_agrt',[product]))
                else:
                    product  += proteins[count+1]
                    reactants = [product,proteins[count+1]]
                    reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'k_agrt',[product]))   
                current_ID[i] = product
                component_dict['protein'].append(product)
                count_protein += 1
            count += 1  
        return 
        
    def protein_dimer_formation(parameter_dict,component_dict,reaction,genes,proteins,intrinsic,current_ID):      
        for i in range(len(genes)):
            reactants = []
            product = ''
            for j in range(intrinsic['Gene_product_connections'][i]): 
                if len(range(intrinsic['Gene_product_connections'][i])) > 1:
                    product += current_ID[i]
                    reactants += [current_ID[i]]
            if len(range(intrinsic['Gene_product_connections'][i])) > 1:               
                reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'k_dim',[product]))
                reaction.append(irreversible(parameter_dict,'ProteinUnbinding',[product],'k_dis',reactants))
                current_ID[i] = product
                component_dict['protein'].append(current_ID[i])
        return 
                    
###########################################################################
    reaction_map  = []
####################### INPUT REACTIONS ###################################
    def input_reactions(reaction,component_dict,parameter_dict,current_ID,input_genes,input_map,input_parameter_dictionary,reaction_map):
        for i in range(len(input_map)):
            for j in input_map[i]:
                if j   == 'direct_activator':  
                    reaction.append(irreversible(input_parameter_dict,'InputActivation',[current_ID[i]],'i',[current_ID[i]+'act']))
                    reaction.append(irreversible(parameter_dict,'ProteinDeactivation',[current_ID[i]+'act'],'i',[current_ID[i]]))
                    reaction_map.append(irreversible(input_parameter_dict,'InputActivation',[current_ID[i]],'i',[current_ID[i]+'act']))
                    current_store = copy.deepcopy(current_ID[i])
                    current_ID[i] = current_ID[i]+'act'
                    component_dict['protein'].append(current_ID[i])      
                elif j == 'direct_deactivator':
                    reaction.append(irreversible(input_parameter_dict,'InputDeactivation',[current_ID[i]],'i',[current_store]))     
                    reaction_map.append(irreversible(input_parameter_dict,'InputDeactivation',[current_ID[i]],'i',[current_store]))
                elif j == 'indirect_activator':
                    inducer = 'i{}'.format(i)
                    reaction.append(irreversible(input_parameter_dict,'InputPassive',[],'input{}'.format(i),[inducer]))
                    component_dict['protein'].append(inducer)
                    reaction.append(irreversible(parameter_dict,'ProteinBinding',[inducer,current_ID[i]],'kP_I_bnd',[inducer + current_ID[i]]))
                    reaction.append(irreversible(parameter_dict,'ProteinUnbinding',[inducer + current_ID[i]],'kP_I_unbnd',[inducer,current_ID[i]]))
                    current_ID[i] = inducer + current_ID[i]    
                    component_dict['protein'].append(current_ID[i])
                    reaction_map.append(irreversible(input_parameter_dict,'InputPassive',[],'input{}'.format(i),[inducer]))                    
                elif j == 'indirect_deactivator':
                    inducer = 'i{}'.format(i)
                    reaction.append(irreversible(input_parameter_dict,'InputPassive',[],'input{}'.format(i),[inducer]))
                    component_dict['protein'].append(inducer)
                    reaction.append(irreversible(parameter_dict,'ProteinBinding',[inducer,current_ID[i]],'kP_I_bnd',[inducer + current_ID[i]]))
                    reaction.append(irreversible(parameter_dict,'ProteinUnbinding',[inducer + current_ID[i]],'kP_I_unbnd',[inducer,current_ID[i]]))               
                    component_dict['protein'].append(inducer + current_ID[i])   
                    reaction_map.append(irreversible(input_parameter_dict,'InputPassive',[],'input{}'.format(i),[inducer]))    
        return 
###########################################################################

####################### INTERACTIVE REACTIONS #############################  

    def passive_activation(reaction,component_dict,parameter_dict,current_ID,interaction_graph):
            for i in interaction_graph['kP_act']:
                reactants = [current_ID[i[0]],current_ID[i[1]]]
                product   = [current_ID[i[0]]+current_ID[i[1]]]
                reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kP_Ap_bnd',product))
                reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kP_Ap_unbnd',reactants))
                component_dict['protein'].append(current_ID[i[0]]+current_ID[i[1]])
                current_ID[i[1]] = current_ID[i[0]]+current_ID[i[1]]
            return 

    def passive_repression(reaction,component_dict,parameter_dict,current_ID,interaction_graph):
            for i in interaction_graph['kP_rep']:
                reactants = [current_ID[i[0]],current_ID[i[1]]]
                product   = [current_ID[i[0]]+current_ID[i[1]]]
                reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kP_Rp_bnd',product))
                reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kP_Rp_unbnd',reactants))
                component_dict['protein'].append(current_ID[i[0]]+current_ID[i[1]])
            return 
            
    def active_activation(reaction,component_dict,parameter_dict,current_ID,interaction_graph):
            for i in interaction_graph['kP_Aa_act']:
                reactants = [current_ID[i[0]],current_ID[i[1]]]
                product   = [current_ID[i[0]]+current_ID[i[1]]]
                reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kP_Ap_bnd',product))
                reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kP_Ap_unbnd',reactants))
                reaction.append(irreversible_conserved(parameter_dict,'ProteinActivation',product,'kP_Ap_act',[current_ID[i[1]] +'A'],[current_ID[i[0]]])) 
                component_dict['protein'].append(current_ID[i[0]]+current_ID[i[1]])
                current_store = copy.deepcopy(current_ID[i[1]])
                current_ID[i[1]] = current_ID[i[1]] +'A'
                component_dict['protein'].append(current_store + 'A')
                reaction.append(irreversible(parameter_dict,'ProteinDeactivation',[current_store +'A'],'kP_Ap_deact',[current_store]))   
            return 

    def active_repression(reaction,component_dict,parameter_dict,current_ID,interaction_graph):
        for i in interaction_graph['kP_Ra_act']:        
            reactants = [current_ID[i[0]],current_ID[i[1]]]
            product   = [current_ID[i[0]]+current_ID[i[1]]]
            reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kP_Ra_bnd',product))
            reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kP_Ra_unbnd',reactants))
            component_dict['protein'].append(current_ID[i[0]]+current_ID[i[1]])
            reaction.append(irreversible(parameter_dict,'Catalysis',product,'kP_Ra_act',[current_ID[i[0]]])) 
        return
    
    def gene_binding(reaction,component_dict,parameter_dict,current_ID,interaction_graph,genes,mRNA_graph,gate):
        def dict_maker(A):   
            graph = dict(enumerate(A))
            graph = dict((v[1],[]) for k,v in graph.items())
            for i in A:
                graph[i[1]].append(i[0]) 
            return graph   
                
        activation_graph = dict_maker(interaction_graph['kG_act'])
        repression_graph = dict_maker(interaction_graph['kG_rep'])               
        activation_keys  = activation_graph.keys()
        repression_keys = repression_graph.keys()
        
        for i in activation_keys:
            if gate[i] == 'AND':
                    reactants = [genes[i]] + [current_ID[j] for j in activation_graph[i]]
                    product   = [genes[i] + ''.join([current_ID[j] for j in activation_graph[i]])]
                    reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kG_A_bnd',product))
                    reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kG_A_unbnd',reactants))
                    reaction.append(irreversible(parameter_dict,'GeneActivation',product,'kG_A_act',mRNA_graph[i]))
                    component_dict['genes'].append(genes[i] + ''.join([current_ID[j] for j in activation_graph[i]]))
            elif gate[i] == 'EXOR':
                    for j in activation_graph[i]:
                        reactants = [genes[i],current_ID[j]]
                        product = [genes[i] + current_ID[j]]
                        reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kG_A_bnd',product))
                        reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kG_A_unbnd',reactants))                    
                        reaction.append(irreversible(parameter_dict,'GeneActivation',product,'kG_A_act',mRNA_graph[i]))                        
                        component_dict['genes'].append(genes[i] + current_ID[j])
                        
        for i in repression_keys:
            if gate[i] == 'AND':
                    reactants = [genes[i]] + [current_ID[j] for j in repression_graph[i]]
                    product   = [genes[i] + ''.join([current_ID[j] for j in repression_graph[i]])]
                    reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kG_A_bnd',product))
                    reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kG_A_unbnd',reactants))
                    component_dict['genes'].append(genes[i] + ''.join([current_ID[j] for j in repression_graph[i]]))
            elif gate[i] == 'EXOR':
                    for j in repression_graph[i]:
                        reactants = [genes[i],current_ID[j]]
                        product = [genes[i] + current_ID[j]]
                        reaction.append(irreversible(parameter_dict,'ProteinBinding',reactants,'kG_A_bnd',product))
                        reaction.append(irreversible(parameter_dict,'ProteinUnbinding',product,'kG_A_unbnd',reactants))       
                        component_dict['genes'].append(genes[i] + current_ID[j])
        return

    def degredation(reaction,component_dict,parameter_dict):
        for i in component_dict['mRNA']:
            reaction.append(irreversible(parameter_dict,'mRNADegredation',[i],'kM_deg',[]))
        for i in component_dict['protein']:
            reaction.append(irreversible(parameter_dict,'ProteinDegredation',[i],'kP_deg',[]))
        return 

#########################Setting Up Reaction Equations ##################################        
    genes                     = base_genes(network,intrinsic)
    mRNA                      = base_mRNA(network,intrinsic,genes)
    proteins,current_ID       = base_components(network,intrinsic,mRNA,current_ID) 
    mRNA_graph                = mRNA_graph(network,intrinsic)
    component_dict['genes']   = genes;
    component_dict['mRNA']    = mRNA;
    component_dict['protein'] = proteins
    
    
    # here you can rearange the way the reactions take place. if the reaction involces a current_ID the order matters for the structure of your final network
    # see appendix for more information on this.

    transcribe(parameter_dict,reaction,genes,intrinsic)
    translate(parameter_dict,reaction,mRNA,proteins,current_ID)
    protein_formation(parameter_dict,component_dict,reaction,genes,proteins,intrinsic,current_ID)
    protein_dimer_formation(parameter_dict,component_dict,reaction,genes,proteins,intrinsic,current_ID)      
    input_reactions(reaction,component_dict,parameter_dict,current_ID,input_genes,input_map,input_parameter_dict,reaction_map)
    passive_activation(reaction,component_dict,parameter_dict,current_ID,interaction_graph)       
    passive_repression(reaction,component_dict,parameter_dict,current_ID,interaction_graph)
    active_activation(reaction,component_dict,parameter_dict,current_ID,interaction_graph)
    active_repression(reaction,component_dict,parameter_dict,current_ID,interaction_graph)
    gene_binding(reaction,component_dict,parameter_dict,current_ID,interaction_graph,genes,mRNA_graph,gate)    
    degredation(reaction,component_dict,parameter_dict)   
    model_components = {'parameters':parameter_dict,'reaction_equations': reaction, 'components': component_dict,'current_ID':current_ID,'inputs':input_parameter_dict,'reaction_map':reaction_map}
    return model_components