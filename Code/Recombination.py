# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 12:44:33 2016

@author: bobvdsluijs
"""
import numpy
import random

#this function performs the recombination of the choses parantal networks. This includes: the binary genes, the rows of the adjacency matrix, the paramaters 
# see manual on how to do recombination 

#Recombination is hard coded for two parents, implementing more parents would mean this function needs to be rewritten with nubmer of parents as a variable
#for a quickfix an added if statement can (i == 2, i == 3 etc) this would require more parents in the dictionary i.e. adjacency_matrix_2 etc..
##########################################################################################################################################
def recombination(selection,operon):
    binary    = lambda n: [random.randint(0,1) for b in range(1,n+1)]    
    maximum   = max(len(selection['genes_0']),len(selection['genes_1']))
    minimum   = min(len(selection['genes_0']),len(selection['genes_1']))
    crossover = binary(random.choice([minimum,maximum]))
    
    #50% probability a crossover = the fittest parent
    if random.random() >0.5:
        crossover = [0]*len(selection['genes_0'])        
        
    adjacency_matrix     = numpy.zeros((len(crossover),len(crossover)), dtype = bool)
    genes                = numpy.zeros((len(crossover),operon), dtype = bool)    
    new_values           = {}  
    # a random list of binary genes is created and it distributes the attributes according to this 
    if len(crossover) == maximum:
        if maximum > len(selection['values_0']):
            for i in range(len(selection['values_0'])-1,maximum,1):
                crossover[i] = 1
        elif maximum > len(selection['values_1']):
            for i in range(len(selection['values_1'])-1,maximum,1):
                crossover[i] = 0
        
    #if recombination is turned the binary list is either all ones or all zeros such the that a parental networks are kept for mutation.        
        
    count = 0
    for i in (crossover):
        if len(crossover) == maximum:
            # you can alter the number of parents here if so desired by addding extra if statement
            if i == 0:
                genes[count] = selection['genes_0'][count]
                adjacency_matrix[count][0:len(selection['adjacency_matrix_0'][count][0:-1])] = selection['adjacency_matrix_0'][count][0:-1]
                new_values.update({count:selection['values_0'][count]})
            elif i == 1:
                genes[count] = selection['genes_1'][count]
                adjacency_matrix[count][0:len(selection['adjacency_matrix_1'][count][0:-1])] = selection['adjacency_matrix_1'][count][0:-1]
                new_values.update({count:selection['values_1'][count]})
        elif len(crossover) == minimum:
            if i == 0:
                genes[count] = selection['genes_0'][count]
                adjacency_matrix[count][0:len(selection['adjacency_matrix_0'][count][0:minimum])] = selection['adjacency_matrix_0'][count][0:minimum]
                new_values.update({count:selection['values_0'][count]})
            elif i == 1:
                genes[count] = selection['genes_1'][count]
                adjacency_matrix[count][0:len(selection['adjacency_matrix_1'][count][0:minimum])] = selection['adjacency_matrix_1'][count][0:minimum]  
                new_values.update({count:selection['values_1'][count]})
        count += 1
#parameter_recombination(crossoverlist,parameterindex_P1,parameterindex_P2): 
    return  {'genes':genes,'adjacency_matrix':adjacency_matrix},new_values  
##########################################################################################################################################