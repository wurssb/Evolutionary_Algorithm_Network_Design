# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 16:06:39 2016

@author: bobvdsluijs
"""  
import copy
import random
import re
import numpy

def random_input_maker(simulation_time,passive_time):
    S = random.random()
    random_input = []
    for i in range(simulation_time):
        if S > 0.5:
            random_input.append(None)
        elif S <= 0.5:
            random_input.append(0)
    return random_input
        

def mutate_input_pattern(patterns,network,intervals):
    S = random.random()
    if S < 0.33:
        network['input'] =  network['input'] + random.choice([-1,1])
    if S > 0.66:
        L = random.random()
        if L > 0.5:
            network['input'] =  random.choice(range(intervals))
    if network['input'] < 1:
        network['input'] = 1
    if network['input'] > intervals:
        network['input'] = max(range(intervals))
    return network
# This script contains functions that build a model after the reaction equations have been generated.

###################### PARAMETERS ##################################################
def assign_parameter_coefficient(model_components,index_basal):
    index = [int(max(0, min(19, random.gauss(10,4)))) for i in range(100)] #assigend integer. the indexation of the integer can be altered default 19.
    new_reactions = copy.deepcopy(model_components['reaction_equations'])
    for i in model_components['reaction_map']:
        temp = new_reactions.index(i)
        del new_reactions[temp]
    ID = [i[1] for i in new_reactions]
    values = {i:random.choice(index) for i in ID}

    # this is the initiliazation function of the reaction rate parameters. 
    # the rate ID's are taken from the reaction rate equations. These are subseqienly assigned an integer and placed in a dictionary
    return values


#This function creates a dictionary whereby the parameters are sorted according to the gene they belong 
#this way they can be passed on to the next generation as the parental networks are recombined
def index_parameters(model_components,network,values):
    initial = {i:[] for i in range(len(network['genes']))}
    index_parameters = copy.deepcopy(initial)
    new_reactions = copy.deepcopy(model_components['reaction_equations'])
    for i in model_components['reaction_map']:
        temp = new_reactions.index(i)
        del new_reactions[temp]
    for i in new_reactions:
        A = list(i[1])
        A = A[0:3] #max 99 genes, were it 4 it would be 999 genes
        index = ""
        for s in A:
            if s.isdigit():
                index += s
        initial[int(eval(index))].append(i[1])
    count = 0
    for i in initial.values():
        for ii in i:
            index_parameters[count].append({ii:values[ii]}) 
        count += 1
    values = {}
    for i in range(len(index_parameters)):
        values.update({i:{}})
        for j in index_parameters[i]:
            values[i].update(j) 
    return values 


#This function mutates a parameter locally at random shifting the coordinate integer either a position up or down
def mutate_parameter_local(values):
   jump  = lambda A: [1  if i >= A else -1 for i in range(20)]
   keys  = values.keys()
   p_type= random.choice(keys)
   if values[p_type] == 0: # if the coordinate integer = 0, the event is always plus 1, no negative values!
       event = 1
   elif values[p_type] ==19: # cannot exceed boundary if coordinate integer  = 19, same rule applies but in reverse is alway minus 1
       event = -1
   else:
       p_dist= jump(values[p_type])
       event = random.choice(p_dist)
   values[p_type] = values[p_type] + event # this is the parameter set with a new parameter value.
   return values
   
# A global mutation a parameter is chosesn at random and a new coordiante integer is assigned.
def mutate_parameter_global(values):
    keys = values.keys()
    p_type = random.choice(keys)
    values[p_type] = random.choice(range(20))
    return values

#This function assigns an acutual value to the coordinate integer based on the boundaries found in main
def assign_parameter_value(values,equation_components,boundaries,index_basal,model_components):
    index          = numpy.arange(0,1,1./20)
    values_ordered = equation_components['parameters']
    transcription  = equation_components['parameters']['Transcribe']
    value          = lambda boundary,coeff: boundary[0]*(1-coeff)+boundary[1]*coeff # the equation that assigns an actual value to the system
    keys           = values_ordered.keys()  
    assigned_values= {}
    for i in keys:
        bounds = boundaries[i]
        for ii in values_ordered[i]:
            assigned_values.update({ii:value(bounds,index[values[ii]])})
    return assigned_values  #dictionary with the real parameter values
 

#assign parameter parameter values set, this function all is almost the same as the previous one. However  one can set rates for specific parameter is ID's are known.
#mostly used for input molecules that need a fixed rate.
def assign_parameter_value_set(values,equation_components,boundaries,index_basal,model_components):
    index          = numpy.arange(0,1,1./20)
    values_ordered = equation_components['parameters']
    transcription  = equation_components['parameters']['Transcribe']
    value          = lambda boundary,coeff: boundary[0]*(1-coeff)+boundary[1]*coeff
    keys           = values_ordered.keys()  
    assigned_values= {}
    for i in keys:
        bounds = boundaries[i]
        for ii in values_ordered[i]:
            assigned_values.update({ii:value(bounds,index[values[ii]])})
    for i in transcription:        
        if eval(i[1]) in index_basal:
            assigned_values[i] = 0
    string = ['i{}kP_deg'.format(j) for j in range(1)] #set the parameter value for tha parameter ID.
    for i in string:
        assigned_values[i] = 0.3
    return assigned_values 
    
    
# Reassign parameter values after recombination hase taken place
# the paremeter set of a new ofspring network is newly generated. The parameter set is subsequently reassigned with the parameters of the recombined networks.
# this is the evolutionary components for the parameters (in that they are remembered)
def reassign_parameters(values,recombined_parameters):
   old_values = {} # create a new parameter dictionary
   for k,v in recombined_parameters.items():
       old_values.update(v)
   old_keys = old_values.keys()
   new_keys = values.keys()
   new_values = {}
   keys = [i for i in old_keys if i in new_keys]
   for i in keys:
       values[i] = recombined_parameters[i]
   for k,v in values.items():
       new_values.update(v)
   return new_values    
    
    
#index parameters is a compensation function in case of a deleted gene mutation
#if a gene is deleted the states and rates have a gap e.g. g1 g2 g3 ... g5 g6 g7 <- because g4 was deleted
# this is not desirable because now the parameter ID's will have to be reassigned. i.e. the rate corresponding to g4 will be applied to g5 which is a no no
#this function decreases the index of g5 g6 g7 and their rates so that the order makes sence again.
def parameter_sort(values,index_mutation): 
    if type(index_mutation) == int:
        return values
    else:
        index_mutation = index_mutation[0]
        
    def minusone(match):
        return str(int(match.group(1)) -1)

    def decreaseString(string, n):
        if n < 10:
            s = '(\d\d+|[{}-9])'.format(n+1)
        else:
            s = '({0}[{1}-9]|[{2}-9]\d+)'.format(n//10, n%10+1, n//10+1)
        return re.sub(s, minusone, string)    
    parameters = {}
    for k,v in values.items():
        k = decreaseString(k,index_mutation)  
        parameters.update({k:v}) # this is the new list of paramter ID's but with the old rates so the parameters are remembered.
    return parameters
    
######################################################################################################################################   
   
 #Where the first part of the script is concerned with the parameters of the system this part of the script is focussed on building the
 # actual ODE system to be solved.
 # this is done according to the equations specified (in the paper)
 # the reaction equations are placed in matrix format: stoichiometric matrix and a flux vector with the rates
        # (Reactants-Products)ˆT * (k*(reactants))   
   
def matrix_construction(model_components,values,plasmid_number):
    components = model_components['components']
    reaction   = model_components['reaction_equations']
    parts      = components.values()
    Node_ID    = [item for sublist in parts for item in sublist]
    Node_ID    = list(set(Node_ID))
    index      = list(enumerate(Node_ID))
    
    def matrix(components,reaction,parts,Node_ID,index):
        reactant_matrix =  numpy.zeros((len(reaction),len(Node_ID)))
        product_matrix  = numpy.zeros((len(reaction),len(Node_ID)))    
        fluxvector = ['  *  '.join(IN) + '  *  ' + parameter for IN,parameter,OUT in reaction]
        # set up the reaction equations in matrix format 
        for i in range(len(reaction)):
            reactants = reaction[i][0]
            for ii in range(len(reactants)):
                for iii in range(len(index)):
                    row, species = index[iii]
                    if species == reactants[ii]:
                        reactant_matrix[i][row] = 1 
        for i in range(len(reaction)):
            reactants = reaction[i][2]
            for ii in range(len(reactants)):
                for iii in range(len(index)):
                    row, species = index[iii]
                    if species == reactants[ii]:
                        product_matrix[i][row] = 1
                        
        #transpose the matrix to obtain the stoichiometric matrix
        M_matrix = ((product_matrix - reactant_matrix)).T
        NodeIDsEquations = copy.deepcopy(Node_ID)
        for i in range(len(Node_ID)):
            TempStore = []
            for ii in range(len(reaction)):
                if M_matrix[i][ii] > 0:
                    TempStore.append("  +  {0}  ".format(fluxvector[ii]))
                if M_matrix[i][ii] < 0:
                    TempStore.append("  -  {0}  ".format(fluxvector[ii]))
            NodeIDsEquations[i] = ''.join(TempStore)
            
        # you obtain a flux vector and an M matrix which multiplied and unpacked give you the ODE system
        return  M_matrix,fluxvector,NodeIDsEquations
        
    # this function actually builds the equations in unison with the Builder equations function below
    def S_stoch(fluxvector,new_state,parametervalues):
        for i in range(len(fluxvector)):
            fluxvector[i] = '  ' + fluxvector[i] + '  '
        for i in parametervalues:
            parametervalues[i] = str(parametervalues[i])
        for key,value in parametervalues.items(): 
            exec '{} = {}'.format(key,value) in locals()
        for i in range(len(fluxvector)):
          for key,value in new_state.items():
              key = ' '+key+' '
              fluxvector[i] = fluxvector[i].replace(key,value)
        for i in range(len(fluxvector)):          
          for key,value in parametervalues.items():
              key = ' '+key+' '
              fluxvector[i]  = fluxvector[i].replace(key,value)
        for i in range(len(fluxvector)):
            if fluxvector[i] == '':
                   fluxvector[i] ='0'
        return fluxvector
        
    # this funciton builds an empty state vector with an assigned variable Y[index] to obtain the appropriate arrays as output for the odeint solver        
    def StateMaker(index):
        States = []
        for i in range(len(index)):
            Variable = list(index[i])
            Variable[0] = 'y[{0}]'.format(i)
            States.append(Variable)
        FinalStates = dict(States)
        new_state = dict(zip(FinalStates.values(),FinalStates.keys()))
        return new_state
    
    # builder of the actual equations that need to be solved so y[0] = k1*y[1]*y[2] - y[0]*k2 etc etc.
    # this is for unpacked parameter values
    def Builder_Equations(equation_system,parametervalues,new_state): 
        for i in parametervalues:
            parametervalues[i] = str(parametervalues[i])
        for key,value in parametervalues.items(): 
            exec '{} = {}'.format(key,value) in locals()
        for i in range(len(equation_system)):
          for key,value in new_state.items():
              key = ' '+key+' '
              equation_system[i] = equation_system[i].replace(key,value)
        for i in range(len(equation_system)):          
          for key,value in parametervalues.items():
              key = ' '+key+' '
              equation_system[i]  = equation_system[i].replace(key,value)
        for i in range(len(equation_system)):
            if equation_system[i] == '':
                equation_system[i] ='0'
        return equation_system  
        
    # this function creates the initial conditions so the first value in you array y[index] for every index. 
    # genes are constant therefore and are equal to the nubmer of plasmids.
    def Maker_initialconditions(new_state,components,plasmid_number):
        States                = new_state
        genes                 = components['genes']
        conserved_states      = []
        index_conserved_states= []
        initial_conditions    = numpy.zeros(len(States))
        for i in range(len(genes)):
            for key,value in States.items():
                if "g{0}".format(i) == key:
                    conserved_states.append(value)  
        for i in range(len(conserved_states)):     
            conserved_states[i] = conserved_states[i].replace("[","")
            conserved_states[i] = conserved_states[i].replace("]","")
            conserved_states[i] = conserved_states[i].replace("y","")     
        index_conserved_states = [eval(conserved_states[i]) for i in range(len(conserved_states))]
        for i in range(len(conserved_states)):      
            initial_conditions[index_conserved_states[i]] = plasmid_number
        for i in range(len(initial_conditions)):
            if initial_conditions[i] == 0: # every componants starts at one however this can be altered
                initial_conditions[i] = 1
        return initial_conditions
    # forthe optimization of for example bistable systems, the initial conditions are a crucial part of the optimziation.
    # you can alter the initial conditions in this function. in the dictiory states you can find the relation between the vector y and the state name.
    # play around with this to alter the initial state.
         
    M_matrix,fluxvector,NodeIDeq = matrix(components,reaction,parts,Node_ID,index)
    System                       = copy.deepcopy(NodeIDeq)
    new_state                    = StateMaker(index)
    equation_system              = Builder_Equations(NodeIDeq,values,new_state)
    stochvector                  = S_stoch(fluxvector,new_state,values)
    IC                           = Maker_initialconditions(new_state,components,plasmid_number)         
    return equation_system,IC,new_state, M_matrix,stochvector,System
   
