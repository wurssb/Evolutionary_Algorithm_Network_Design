# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 16:17:55 2016

@author: bobvdsluijs
"""

import numpy
import random
import itertools
import copy   
    
# the graph file does the following.
#    1.) it creates the intitial networks (called in main)
#    2.) it contains all the functions for a mutation (everything is in numpy for computational efficacy) 
#    3.) it creates a graph structure in which all interactions between genes are mapped (first number is operator, second nubmer is operated on) 
#           and sorted according to reaction type such that the right reaction equations are created
         
############  Initial set up of parental networks ###############

def initial_setup(initial_gene_number,operon):
#define which genes are input genes and which are output genes they cannot be deleted or mutated
    gene_pool = numpy.random.randint(2,size = operon)    
    for i in range(initial_gene_number-1):
        gene_pool = numpy.vstack((gene_pool,numpy.random.randint(2,size = operon))) 
    interactions = numpy.zeros((initial_gene_number,initial_gene_number),dtype = bool)
    network = {'genes':gene_pool,'adjacency_matrix':interactions}
    return network

#This results in a dictionary {genes: numpy.array, interaction matrix: numpy.array} 
#which containts all information given the rule key (order) except for paramter values
##################################################################  

############  All functions that modify either the binary sequence of genes or adjacency matrix ###############

def add_gene(network,operon):
    network['genes'] = numpy.vstack((network['genes'],numpy.random.randint(2,size = operon)))
    network['adjacency_matrix'] = numpy.c_[network['adjacency_matrix'],numpy.zeros(len(network['adjacency_matrix'][:,0]),dtype = bool)]
    network['adjacency_matrix'] = numpy.vstack((network['adjacency_matrix'],numpy.zeros(len(network['adjacency_matrix'][0]),dtype = bool)))
    select = random.choice(range(len(network['adjacency_matrix'][-1])))
    network['adjacency_matrix'][-1,select] ^= True
    return network,None
    
def del_gene(network,input_genes,output_genes):
    select = random.choice(range(input_genes+output_genes,len(network['adjacency_matrix']),1))
    network['genes'] = numpy.delete(network['genes'],select,axis=0)
    network['adjacency_matrix'] = numpy.delete(network['adjacency_matrix'],select,axis=1)
    network['adjacency_matrix'] = numpy.delete(network['adjacency_matrix'],select,axis=0)
    return network,select
    
def mutate_gene(network,operon):
    select = (random.choice(range(len(network['adjacency_matrix'][0]))),random.choice(range(operon)))
    network['genes'][select[0],select[1]] ^= True
    return network,None
    
def add_connection(network):
    indices = numpy.nonzero(network['adjacency_matrix'] == 0)
    slices  = numpy.vstack(indices)
    select  = random.choice(range(len(slices[0])))
    slice_select = slices[:,select]
    network['adjacency_matrix'][slice_select[0],slice_select[1]] ^= True
    return network,None 
    
def del_connection(network):    
    indices = numpy.nonzero(network['adjacency_matrix'])
    slices  = numpy.vstack(indices)
    select  = random.choice(range(len(slices[0])))
    slice_select = slices[:,select]
    network['adjacency_matrix'][slice_select[0],slice_select[1]] ^= True
    return network,None   
    
def move_connection(network):
    indices = numpy.nonzero(network['adjacency_matrix'])
    slices  = numpy.vstack(indices)
    select  = random.choice(range(len(slices[0])))
    slice_select = slices[:,select]    
    network['adjacency_matrix'][slice_select[0],slice_select[1]] ^= True    
    new_select = random.choice(range(len(network['adjacency_matrix'][0])))
    network['adjacency_matrix'][slice_select[0],new_select] ^= True                    
    return network,None
################################################################## 
    
#################### Function that mutates an individual ###########################     
def event(network,operon,input_genes,output_genes,event_probability,max_genes,min_genes):
    indices       = numpy.nonzero(network['adjacency_matrix'])
    slices        = numpy.vstack(indices)
    gene_number   = len(network['adjacency_matrix'])
    selectrange = range(6)    
    distribution = []
    for i in range(len(event_probability)):
        P = int(event_probability[i]*100) #create array of nubmer from distribution and select
        for iteration in range(P):
            distribution.append(selectrange[i])
    function      = {0:'add_gene(network,operon) ',1:'mutate_gene(network,operon)',2:
    'add_connection(network)',3:' del_connection(network) ',4:' move_connection(network) ',5:'del_gene(network,input_genes,output_genes)'} #operation corresponding to number 
    if numpy.all(network['adjacency_matrix']) == True:
        distribution =filter(lambda a: a != 2, distribution)        
    if len(slices[1]) < 1:
        distribution = filter(lambda a: a != 3,distribution)
        distribution = filter(lambda a: a != 4,distribution)
    if gene_number <= min_genes:
        distribution = filter(lambda a: a != 5, distribution)
    if gene_number >= max_genes:
        distribution = filter(lambda a: a != 0, distribution)
    select = random.choice(distribution)
    return eval(function[select]) 
#########################################################################                     
      
#################### Function Creates Graph structure ###########################        
def create_graph(network,order,Promoter_gate,Promoter_interaction_type,Gene_product,Promoter_expression_type,Gene_product_subunits,Gene_product_connections,pool):
# find the intrinisc features of the components
    gate = {}; interaction_type = {}; expression_type = {};product = {}; subunits  = {}; connections  = {}   
    for i in range(len(network['genes'])):
        pg = tuple(network['genes'][i][0:Promoter_gate])
        pi = tuple(network['genes'][i][Promoter_gate:Promoter_gate+Promoter_interaction_type])
        pe = tuple(network['genes'][i][Promoter_gate+Promoter_interaction_type:Promoter_gate+Promoter_interaction_type+Promoter_expression_type])
        gp = tuple(network['genes'][i][Promoter_gate+Promoter_interaction_type+Promoter_expression_type:Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product])
        gs = tuple(network['genes'][i][Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product:Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product+Gene_product_subunits])
        gc = tuple(network['genes'][i][Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product+Gene_product_subunits:Promoter_gate+Promoter_interaction_type+Promoter_expression_type+Gene_product+Gene_product_subunits+Gene_product_connections])

        a=order['Promoter_gate'][pg]
        b=order['Promoter_interaction_type'][pi]
        c=order['Promoter_expression_type'][pe]
        d=order['Gene_product'][gp]
        e=eval(order['Gene_product_subunits'][gs][1])
        f=eval(order['Gene_product_connections'][gc][1])
       
        gate.update({i:a}) 
        interaction_type.update({i:b})
        expression_type.update({i:c})
        product.update({i:d})
        subunits.update({i:e})
        connections.update({i:f})

    intrinsic = {'Promoter_gate':gate,'Promoter_interaction_type':interaction_type,'Promoter_expression_type':expression_type,
    'Gene_product':product,'Gene_product_subunits':subunits,'Gene_product_connections':connections}
    count = 0
    for i in network['adjacency_matrix']:
        if all(i==False):
            network['adjacency_matrix'][count][random.choice(range(len(i)))] = True
        count += 1  

          
    ##################################################### 
    # If you want to modify the reactions, which results in certain interactions not being allowed here they can be excluded
    PR = []
    for i in range(len(product)):
        if product[i] == 'PR_A' or product[i] == 'PR_R': # a reaction between 2 inhibitors is not allowed
            PR.append(i)
    for i in PR:
        network['adjacency_matrix'][i][i] = False 
                
    PR = []
    for i in range(len(product)):
        if product[i] == 'PR_A' or product[i] == 'PR_R':
            PR.append(i)
    C = list(itertools.product((PR),repeat = 2))
    for i,j in C:
            network['adjacency_matrix'][i][j] = False
    #####################################################
    #####################################################        
            
    # for certain interactions i.e. if gene product = AR and promoter = AR then the gene is activated 
    indices = numpy.nonzero(network['adjacency_matrix'])
    interaction_vector = numpy.vstack(indices) 
    interactions       = ['kP_act','kP_rep','kG_act','kG_rep','kP_Aa_act','kP_Ra_act']
    graph_structure  = {}
    for i in interactions:
        graph_structure.update({i:[]}) 
    count = 0
    for i in range(len(indices[0])):
            if product[indices[0][count]]   == 'irr_act':
                graph_structure['kP_Aa_act'].append(interaction_vector[:,count])
                
            elif product[indices[0][count]] == 'irr_rep':                   
                graph_structure['kP_Ra_act'].append(interaction_vector[:,count])        
        
            elif product[indices[0][count]]   == 'PR_A':
                graph_structure['kP_act'].append(interaction_vector[:,count])
                
            elif product[indices[0][count]] == 'PR_R':                   
                graph_structure['kP_rep'].append(interaction_vector[:,count])
                         
            elif product[indices[0][count]] == 'AR' and interaction_type[indices[1][count]] == 'AR':    
                graph_structure['kG_act'].append(interaction_vector[:,count])                 
    
            elif product[indices[0][count]] == 'RA' and interaction_type[indices[1][count]] == 'RA':    
                graph_structure['kG_act'].append(interaction_vector[:,count])  
            
            elif product[indices[0][count]] == 'AR' and interaction_type[indices[1][count]] == 'RA':    
                graph_structure['kG_rep'].append(interaction_vector[:,count]) 
                
            elif product[indices[0][count]] == 'RA' and interaction_type[indices[1][count]] == 'AR':    
                graph_structure['kG_rep'].append(interaction_vector[:,count])  
            count += 1        
        
    index_basal = [i for i in range(len(expression_type)) if expression_type[i] == 'Facultative']
    return graph_structure,intrinsic,index_basal
    

        
        