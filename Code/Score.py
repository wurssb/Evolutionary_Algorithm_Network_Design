# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 10:23:46 2016

@author: bobvdsluijs
"""

from __future__ import division
import numpy
import math 
import copy
import random
import matplotlib.pylab as plt
from scipy.signal import argrelextrema
import itertools as it
from scipy import stats

from numpy import array 
import sympy
from scipy.fftpack import rfft
from collections import Counter

# The scoring function is the most important part of the algorithm in that it sets the shape of the fitness landscpae and thereby your ability
# to find the system behavior that you desire.
# In here I have a single example of a scoring function for an oscillating system 
# For the scoring we work with the time-series data obtained from the simulation whether stochastic or deterministic
# Stored in numpy arrays this data can be manipulated to suit your scoring function (it is as you please)
# Be sure to do a thorough test of your own scoring function. It is likely that it will result in unforseen concentration profiles.

# Requirement:
#    for the selection it has to be stored in a list within a dictionary.:
#it it has to be a minimization of the score (if not raise final score to the power of -1) 
def scoring_oscillations(new_state,model_components,result,output):
    score_dict    = {'objective_score': []} #here you can add extra scoring objectives, e.g. {'objective_score': [], 'peak_hight':[]}
    protein_ID    = [model_components['current_ID'][i] for i in output]
    index_data    = [new_state[i] for i in protein_ID]
    index         = []
    for i in index_data:
        i = i.strip('y')
        i = i.strip(']')
        i = i.strip('[')
        index.append(eval(i))
    datas = [result[:,i] for i in index]    
    data = [i[1200:] for i in datas]    
    
    minima = [argrelextrema(i, numpy.less) for i in data]
    maxima = [argrelextrema(i, numpy.greater) for i in data] 
    
    Minima = []
    Maxima = []    
    for i in minima:
        L = []
        for k in i[0]:
            L.append(k)
        Minima.append(L)
    for i in maxima:
        L = []
        for k in i[0]:
            L.append(k)
        Maxima.append(L)
    extrema = [Minima[i] + Maxima[i] for i in range(len(Minima))]
    extrema_detection = []
    count     = 0
    for i in range(len(data)):
        sumfactor = 0
        score     = 20
        for j in range(len(extrema[count])-2):
            numerator   = abs(data[i][extrema[count][j]] - data[i][extrema[count][j+1]])
            denominator = data[i][extrema[count][j]] + data[i][extrema[count][j+1]]
            factor      = min(1, abs(data[i][extrema[count][j]] - data[i][extrema[count][j+1]]))
            sumfactor   += (float(numerator)/float(denominator))*factor
        score -= 2*sumfactor
        count += 1
        extrema_detection.append(float(score)/float(len(data)))
    score_dict['objective_score'].append(sum(extrema_detection))
    return score_dict,datas










