# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 12:54:36 2016

@author: bobvdsluijs
"""
import matplotlib.pylab as plt 
import copy

def store_generation(network,value,score,data,p_values):
    return {'network':network,'value':value,'score':score,'data':data,'param_values':p_values}   

def store_plot(objective,result,simulation_time,path,iteration,search):
        fig = plt.figure()
        for i in range(len(objective)):
            plt.xlabel('convergence generation ' + str(iteration))
            plt.plot(simulation_time,result[i])
            plt.plot(simulation_time,objective[i], color = 'k')
        targetfile = path + '\pulse' + str(search)
        plt.savefig(targetfile)
        plt.close(fig)
        return
        
def store_dictionary(parameter_dictionary,path):
    with open(path + '\dictionary_output\variables_pulse.txt' , "a") as outfile:
        outfile.write(str(parameter_dictionary))
        outfile.write("\n")
    return
    
def store_network(order,objective_network,final_network):
    network = copy.deepcopy(final_network)
    del network['data']
    new_network = {'order':order,'objective':objective_network,'network':network}
    with open(path + '\dictionary_output\network_pulse.txt' , "a") as outfile:
        outfile.write(str(new_network))
        outfile.write("\n")        
    return 
    
def store_data(path,data,iteration):
    with open(path + '\randomized_objective\objective{}.txt'.format(iteration), 'w') as outfile:
        outfile.write(str(data))
    return
    
def store_result(order,pool,objective,final_network,parameters):
    network = copy.deepcopy(final_network)
    del network['data']
    new_network = {'order':order,'objective':objective_network,'network':network}
    with open(path + '\dictionary_output\network_pulse.txt' , "a") as outfile:
        outfile.write(str(new_network))
        outfile.write("\n")        
    return    
    