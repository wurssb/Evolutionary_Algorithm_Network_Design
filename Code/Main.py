 # -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 16:16:27 2016

@author: bobvdsluijs
"""
import random
from Setup import *
import matplotlib.pylab as plt
import os
    

def store_network(store_dict,path):  
    writepath = path
    mode = 'a' if os.path.exists(writepath) else 'w'   #If the file exists it APPENDS it to the existing file instead of writing a new one
    with open(writepath,mode) as outfile:
        outfile.write(str(store_dict))
        outfile.write("\n")   
    return
 
# This is the main run file.
# This script allows you to set the boundaries,network components,algorithm parameters and algorithm subroutines in the following order.

# 1.) set the components i.e. reaction equations you would like in your network
# 2.) set the parameter boudnaries for reaction types e.g. catalysis, protein-protein binding transport etc
# 3.) set boundaries on the network
# 4.) set the simulation time in minutes! and the size of the parts of the binary gene (as shown in the pool)
# 5.) set mutation probabilities. That being the relative frequency with which a gene is deleted compared to added 
# 6.) set algorithm subroutines and the number of individuals in a population (for large networks it is worthwile to increase the nubmer of individuals) and designated objectives
# 7.) Create and input pattern if so desired.
# 8.) set a predefined concentration profile as an objective. (size of the objective has to equal the size of the simulation)
# 9.) set number of algorithm runs and location for storage of data.

#pool gives relativa frequencies with which a reactiion occurs. if a component undesirable set to zero!

pool       = {'Promoter_gate':{'AND':1,'EXOR':1},                            # the promoter gate, no AND gate in the networks in this example  
              'Promoter_interaction_type':{'RA':1,'AR':1},                   # promoter-tanscriptionfactor interaction code, remove one to restrict transcription factor duality
              'Promoter_expression_type':{'Constitutive':4,'Facultative':0}, # promoter-tanscriptionfactor interaction code, remove one to restrict duality
              'Gene_product':{'PR_A':5,'PR_R':5,'RA':20,'AR':20,"irr_act":10,"irr_rep":10},    # set the frequency with which a protein operator occurs AR RA are transcription factors. PR_A/R protein regulators (activator repressor) 
              'Gene_product_subunits':{'s1':20,'s2':20,'s3':0,'s8':0},      # set size Protein with subunits accopanied by mRNA transcript that forms final protein, the s1 s2.. signifies number of transcripts and can be alterred as wel to say s10
              'Gene_product_connections':{'s1':20,'s2':20,'s3':0,'s4':0}}   # same as previous but for dimerization  


#boundaries are the parameter boundaries for the system and are completely user defined.
boundaries = {'Transcribe':(1,12) ,'Translate':(1,12),'ProteinBinding':(0.1,100),'ProteinUnbinding':(0.1,100),
              'ProteinActivation':(100,1000) ,'ProteinDeactivation':(100,1000) ,'GeneActivation':(1,60),'Catalysis':(100,1000),'Transport':(1,10) 
             ,'ProteinDegredation':(0.1,0.9),'mRNADegredation':(0.5,0.95),'InputActivation':(1,10) ,'InputDeactivation':(1,10)}

compartments        = 0   #redundant factor in this version stick to zero
plasmid_number      = 1   # set number of plasmids
output_genes        = 1   # set number of output genes
input_genes         = 1   # set nubmer of input genes
min_genes           = 4   # set minumum number of genes in a networks 
max_genes           = 10  # set maximum number of genes in network
initial_gene_number = input_genes + output_genes +3


simulation_time                 = 200  #set simulation time in MINUTES!
Promoter_gate                   = 3    # length of binary gene subuntis as shown in pool (dont make to long, number of allocated substrings increases factorially)
Promoter_interaction_type       = 5 
Gene_product                    = 5 
Promoter_expression_type        = 3 
Gene_product_subunits           = 3 
Gene_product_connections        = 3 
operon                          = Promoter_gate + Promoter_interaction_type + Gene_product + Promoter_expression_type + Gene_product_subunits + Gene_product_connections

# these settings yield a gene length of 22 so len([01010101.....1]) = 22


add_gene            = 0.3 #probability that a gene is added etc.
mutate_gene         = 0.3
add_connection      = 0.3
del_connection      = 0.4
move_connection     = 0.4
del_gene            = 0.4
event_probability   = [add_gene,mutate_gene,add_connection,del_connection,move_connection,del_gene] 

    
input_map           = {} #{0:['direct_activator']}  #direct_activator direct_deactivator indirect_activator indirect_deactivator
input_type          = 'direct'
input_pattern       = [None]*simulation_time 


      
#input_pattern[time_of_input[1]:time_of_input[1]+length_of_input] = numpy.ones((length_of_input)) 



mutation_method     = 'global'             # can be: local             global               both 
selection_method    = 'semi_proportional'  # can be: proportional     semi_proportional     elite
parameter_mutation  = 3                    # number of parameter mutation each individual undergoes
network_mutation    = 1                    # number of networks mutations each individial undergoes after recombination     
objective_number    = 1                    # number of objectives
offspring_number    = 10                   # number of individuals in a generation 
perturbe            = 'on'                 # perturbe the system after 150 generations of the the same parent

max_iteration       = 2000                 # maximum number of generations in a run of the algortihm i,e. algorithm is terminated after this many genetions 
path                = ''   #destination folder result
output              = [0]                                                                     #output gene a list of genes IN ORDER whose product is scored againts the objective, largest integer minus 1 cannot exceed minimum gene number
order               = set_rules(Promoter_gate,Promoter_interaction_type,Gene_product,Promoter_expression_type,Gene_product_subunits,Gene_product_connections,pool) #distributes binary substrings over components acording to the distribtution in the pool
initial_network     = initial_setup(initial_gene_number,operon) #initail parental network. This can be adjusted so that the parental networks are designed according to you specificaitons
objective           = 0



algorithm_runs = 1000                                  # number of times the algorithm is run
for i in range(algorithm_runs):
    result,simulation_time,iteration,score_track,generation_select = algorithm_oscillations(max_iteration,path,i,order,
              initial_network,objective,offspring_number,pool,boundaries,
              Promoter_gate,Promoter_interaction_type,Gene_product,
              Promoter_expression_type,Gene_product_subunits,Gene_product_connections,
              operon,compartments,output_genes,event_probability,input_map,
              input_genes,initial_gene_number,plasmid_number,selection_method,
              mutation_method,parameter_mutation,network_mutation,objective_number,
              max_genes,min_genes,simulation_time,
              input_type,input_pattern,output)
              
              
    new = copy.deepcopy(generation_select)
    del new[0]['data']  # Dont want to store time series data 
    del new[1]['data']  # Dont want to store time series data
    store_dict = {'iteration':iteration,'order':order,'pool':pool,'generation_select': new,'output':output,'score':score_track.tolist()} #Storing it as a printable dictiory to track progress, can be modified to store properly
    store_network(store_dict,path)
    
