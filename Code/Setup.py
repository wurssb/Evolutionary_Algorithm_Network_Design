# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 11:00:03 2016

@author: bobvdsluijs
"""
import random
import numpy
import copy
import multiprocessing 
import matplotlib.pylab as plt

from Rules import *
from Graph import *
from Score import *
from Select import *
from Build import *
from Recombination import *
from Store import *
from Reactions import *
from Solve import *

#this script contains two functionsm one contains the overall structure of the algorithm with a generation function where
#two parents undergo all the operations in this function, recombination,mutation, solving scoring etc etc i.e. it functions at the level of the individual

#This main function is the algorithm and works on the generational level.
#the generation is a dictionary with all the infomration pertaining to those individials in that generation 
#this is fed into the selection function to obtain a generation_select i.e. two parents.


def algorithm_oscillations(max_iteration,path,iteration,order,
              initial_network,objective,offspring_number,pool,boundaries,
              Promoter_gate,Promoter_interaction_type,Gene_product,
              Promoter_expression_type,Gene_product_subunits,Gene_product_connections,
              operon,compartments,output_genes,event_probability,input_map,
              input_genes,initial_gene_number,plasmid_number,selection_method,
              mutation_method,parameter_mutation,network_mutation,objective_number,
              max_genes,min_genes,simulation_time,
              input_type,input_pattern,output):                            

    generation      = {}
    seperation_dict = {} 
    generation      = {}
    score_track     = numpy.zeros((max_iteration+1))  

    # here you can set the conditions for terminations right now the conditions are set  for number of generations and a high score that is 'good enough'
    iteration       = 0
    high_score      = 20        
    while iteration <= max_iteration:
            print iteration
            generation = {offspring:EA_oscillations(max_iteration,path,iteration,order,
              initial_network,objective,offspring_number,pool,boundaries,
              Promoter_gate,Promoter_interaction_type,Gene_product,
              Promoter_expression_type,Gene_product_subunits,Gene_product_connections,
              operon,compartments,output_genes,event_probability,input_map,
              input_genes,initial_gene_number,plasmid_number,selection_method,
              mutation_method,parameter_mutation,network_mutation,objective_number,
              max_genes,min_genes,simulation_time,
              input_type,input_pattern,output,seperation_dict) for offspring in range(offspring_number)} #offspring number creates number of individuals in this generation          
            if iteration != 0:
                generation.update(copy.deepcopy(generation_select))
                
            final_rank                  = rank_oscillations(generation,selection_method)
            generation_select           = selection(final_rank,selection_method,generation) 
            seperation_dict,high_score  = seperation(generation_select,path)
            
            # you can play around with other termination criteria, here we do a simple score track
            #if so desired we can state that if the score remains unchanged for x generations the algorithms is terminated 
            #it also allows you to see the progression
            if high_score >= 0:
                datafile = copy.deepcopy(generation_select[0]['data'])
                score_track[iteration] = high_score
                # to check scoring function it may be beneficial to plot each generation during testing to see whether appropriate dynamics are being obtained.
                #if iteration%1 == 0:
                #    plt.plot(datafile[0])
                #    plt.show()
                new = copy.deepcopy(generation_select)
                del new[0]['data']
                del new[1]['data']
                del new[0]['value']
                del new[1]['value']
                store_dict = {'iteration':iteration,'order':order,'pool':pool,'generation_select': new,'output':output,'score':score_track.tolist()}
                store_EA(store_dict,path,run)
                iteration+= 1
                print iteration,score_track.tolist()
            else:
                iteration = max_iteration+1
                
    return datafile[0][0],simulation_time,iteration,score_track,generation_select

def store_EA(store_dict,path,search):
    writepath = path + '.txt'.format(search)
    mode = 'a' if os.path.exists(writepath) else 'w'
    with open(writepath,mode) as outfile:
        outfile.write(str(store_dict))
        outfile.write("\n")
    return


def EA_oscillations(max_iteration,path,iteration,order,
              initial_network,objective,offspring_number,pool,boundaries,
              Promoter_gate,Promoter_interaction_type,Gene_product,
              Promoter_expression_type,Gene_product_subunits,Gene_product_connections,
              operon,compartments,output_genes,event_probability,input_map,
              input_genes,initial_gene_number,plasmid_number,selection_method,
              mutation_method,parameter_mutation,network_mutation,objective_number,
              max_genes,min_genes,simulation_time,
              input_type,input_pattern,output,seperation_dict):
          

        #the initial parents are recombined
        if iteration > 0:       
            initial_network,recombined_parameters = recombination(seperation_dict,operon)
            
        #the recombined individual is mutated x number of times
        for i in range(network_mutation):
            network,index_mutation       = event(copy.deepcopy(initial_network),operon,input_genes,output_genes,event_probability,max_genes,min_genes)    

        
        #Create the interaction graph based on genes and network.....generate reaction equations......create parameter values
        interaction_graph,intrinsic,basal= create_graph(network,order,Promoter_gate,Promoter_interaction_type,Gene_product,Promoter_expression_type,Gene_product_subunits,Gene_product_connections,pool) 
        model_components                 = reaction_equation(network,interaction_graph,pool,intrinsic,input_map,input_genes) 
        values                           = assign_parameter_coefficient(model_components,basal) 

        co_values = copy.deepcopy(values)                
        #assign the actual parameter values
        #index the values
        index_values = index_parameters(model_components,network,co_values)  
        
        #sort the parameters of the previous generation
        if index_mutation != None and iteration > 0:
            recombined_parameters = parameter_sort(recombined_parameters,index_mutation)
        #'overlay' the parameters of the previous generation
        if iteration > 0:
            values = reassign_parameters(copy.deepcopy(index_values),recombined_parameters)   
            
        #mutate the parameters either locally, globally or both for parameter_mutation number of times
        for i in range(parameter_mutation):
            if mutation_method == 'local':
                values = mutate_parameter_local(values)
            if mutation_method == 'global':    
                values = mutate_parameter_global(values)
            if mutation_method == 'both':
                values = eval(random.choice(['mutate_parameter_local(values)','mutate_parameter_global(values)']))
        
        values = assign_parameter_value(values,model_components,boundaries,basal,model_components)         
        # Create matrix and model equations for the odeint solver. for the stochastic solver, the M_matrix and stoch vector are inputs instead of the equation_system
        equation_system,IC,new_state,M_matrix,stochvector,System = matrix_construction(model_components,values,plasmid_number)

        #numerically integrate the system equations.
        result = solver_odeint(equation_system,IC,simulation_time)

        # score the resuly with ones own scoring function 
        score,data  = scoring_oscillations(new_state,model_components,result,output)
        #sort the generation to obtain the approapriate format for selection 
        generation  = store_generation(network,index_values,score,data,values)
        return generation


def seperation(generation_select,path):
    high_score = generation_select[0]['score']['objective_score'][0]
    values_0 = generation_select[0]['value']
    values_1 = generation_select[1]['value']
    genes_0  = generation_select[0]['network']['genes']
    genes_1  = generation_select[1]['network']['genes']
    adjacency_matrix_0 = generation_select[0]['network']['adjacency_matrix']
    adjacency_matrix_1 = generation_select[1]['network']['adjacency_matrix']
    return {'genes_0':genes_0,'genes_1':genes_1,'values_0':values_0,'values_1':values_1,'adjacency_matrix_0':adjacency_matrix_0,'adjacency_matrix_1':adjacency_matrix_1},high_score
    
    