# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 12:51:47 2016

@author: bobvdsluijs
"""

import copy
import random


#this file contains the rank functions corresponding to the diferent scoring function
#every SCORING fucntion has a RANK function


def selection(final_rank,selection_method,generation):
        probabilities = [int(100)/(i+1) for i in range(len(final_rank))]   
        if selection_method == 'elite':
            generation_select = {0:generation[final_rank[0]],1:generation[final_rank[1]]}
        elif selection_method == 'semi_proportional':
            temporary = copy.deepcopy(final_rank)
            vector    = []
            del temporary[0]
            for i in temporary:
                vector += [i for j in range(int(probabilities[i]))]
            generation_select = {0:generation[final_rank[0]],1:generation[random.choice(vector)]}
        elif selection_method == 'proportional':
            temporary = copy.deepcopy(final_rank)
            vector = []
            for i in temporary:
                vector += [i for j in range(int(probabilities[i]))]
            generation_select = {0:generation[random.choice(vector)],1:generation[random.choice(vector)]}
            
#this function selection. sorts out the dictionary based on the rank that was calculated for the scoring function
#this function does not need to be altered. as inputs it takes the generational input. the selection method, and the list with final ranks
#as long as these inputs are given any score and ranking function can be applied.
        return generation_select
        

def rank_oscillations(generation,selection_method):
        scores = [generation[i]['score'] for i in range(len(generation))]
        oscillation_score   = [i['oscillation_score'] for i in scores]

        rank_oscillation_score = sorted(range(len(oscillation_score)), key=lambda k: oscillation_score[k])

        A = list(rank_oscillation_score)

        rank = []
        for i in range(len(oscillation_score)):
           a  = A.index(i)
           rank.append(a)
        final_rank = sorted(range(len(rank)), key=lambda k: rank[k])  
        return final_rank
  