README File: Evolutionary Algorithm Functions. 

Details about usage and the resulting analysis with this algorithm can be found in ‘Designing Synthetic Networks and Experimentation in silico: A Generalised Evolutionary Algorithm Approach’ by Smith, van Sluijs and Fleck.

For detailed questions contact the authors:
Robert Smith; robert1.smith@wur.nl / r.w.smith.ncl09@googlemail.com
Christian Fleck; christian.fleck@wur.nl

In the folder:

Package - the main files required to run the EA.
Extensions - scripts that can be used for extended analysis depending on the problem.

----------

Basic instructions for use.

In the 'Code' folder there are a number of subroutines that need to be edited for specific problems. As an example, the attached code was used to find oscillating systems.

1. main.py: Here, the options for what the EA is allowed to do need to be selected.
This includes editing the gene pool (i.e. do you want to add a protein with a special function or not allow mRNA to be considered in the system as could be the case for post-translational oscillators).
Check the parameter ranges and their respective units.
Set any input signals (i.e. light or growth source, etc.).

2. graph.py:
For any new system components added in main.py, the rules for these components function need to be added into this file.
At the bottom of the file is an ordered list of reactions, this can also be edited if specific reactions are known to occur before others (e.g. transcription of DNA to mRNA occurs before translation of mRNA to protein, as a simple example)

3. reactions.py:
Here new reactions can be added or removed.
For example, if you want to analyse post-translational mechanisms only, then you can remove mRNA from the model.

4. build.py:
In this file, initial conditions for simulations can be changed.

5. solve.py:
Here you can edit the simulation conditions for each model, e.g. simulated time, solver used, etc.

6. score.py:
Here the scoring function for your optimisation needs to be written.

7. setup.py:
This is, arguably, the most important file, where the EA algorithm brings each of the individual steps together and stores each generation.
You can also print output as the EA functions so that you can check that everything is functioning correctly.

8. Post-Analysis:
Phylogenetic_Tree_EA.py:
This is a test code to analyse the output from the EA. Parts of the code need to be edited to match the options selected in main.py.
The output provides you with an evolutionary trajectory of the fitness score and a set of networks that show the evolution from the initial network to the optimal system are stored.

simulate_output.py:
Here the dynamics of networks can be plotted so that network behaviour can be observed.

----------

File - main.py:
	The main file contains most of the inputs that are applied to the algorithm, inputs that are fixed for the duration of the algorithm such as mutation rates, selection methods or applied to the dynamics of the networks themselves-input patterns. 

This file contains the following function:
	store_(name)_network(dictionary to be stored, file path):
	This function stores the data of the run, the user can decide what they want to store and how they want to store it by unpacking the dictionary.

This file calls the following function:
	algorithm_(name)(all the inputs that go into the algorithm).
	

In the main file there is a function called order. The order function takes the attributes in the pool variable and distributes these attributes over different permutations of ones and zeros for a fixed length that is user defined per attribute class (see the Supplementary Material for further details). The resulting variable 'order'   acts like a master key that can translate the binary genes and the interactions between them in the appropriate reaction equations.


File - setup.py:
The setup file calls all the functions that are applied over the course of the algorithm in the right order given certain conditions i.e. the number of generations, the attained score etc.
	
This file contains two functions:

1-algorithm_(name)(all the inputs that go into the algorithm).
This is the algorithm function that is called in main and contains the results of the generation of network models and the criteria for termination. This criteria can be altered to suit the user’s needs. We refer the user to ‘Evolutionary Optimisation Algorithms’ by Dan Simon (2013) for applying appropriate termination criteria. 

2-EA_(name)_ordered(all inputs concerning the operations of the algorithm). This function contains all the functions/operations that are applied in to a single individual within a generation. 

We will go through the operations in order of function calls:

1) recombination(seperation_dictionary,operon)

This is the first operation in the algorithm. The rows in the adjacency matrix and the genes are indexed in accordance with position of the (0s and 1s) that are found In the recombination list, where 0's refer to the nodes and interaction found in the first first parental network and the 1s for the second parental network. This function can be altered to state that with a probability p, the recombination list is either all ones or all zeros preserving network structure and reducing the variability of the networks within and over the course of the generations. 

2) event(initial network, operon, input, genes, output, event probability, maximum number of genes, minimum number of genes):

The event function is the second operation in the algorithm, it concerns a mutation event applied 	to the individual that was created after the recombination of the selected parents. This function randomly selects for 1 of 6 mutations to occur within certain boundaries and according to user defined probabilities (the event probability input) as chosen in the main file.

Gene mutation (flipping a bit in a binary genes after randomly selecting one), gene addition and gene deletion. This adds a binary gene and an accompanying row in the adjacency matrix or it deletes one. The output gene cannot be deleted (hence output is an input into the function) similarly the probability of these events goes to zero if the network is either the maximum allowed size (for gene addition) or the minimum allowed size (for gene deletion). The maximum and minimum number of genes input serve as an input to that end. Other mutation include moving, adding or deleting a connection in the network.

3) create_graph(network, order, promoter gate, etc,) (etc <- different sections of the binary gene used for properly translating the networks with the order variable.)
	
The create graph function takes the network with the binary genes, i.e. the information as to how nodes operate on other nodes and how they are operated upon. The adjacency matrix containing the information regarding which nodes interact with which nodes and translates these into actual reactions with reaction types with a KEY inputted as the variable 'order’. This order variable is used to organise all interactions along reaction types given the interactions that are present in the reaction library. The graph structure is a dictionary ordered along reaction types and reaction partners in those reaction types. 

4) reaction_equation(network, interaction graph, pool of possibilities, intrinsic, inputs pattern,input,genes):

This function comes after the creation of the interaction graph (ordered by reaction type) and creates a set of reaction equations based of-of these in the following format [[inputs],rate,[outputs]]. These reaction equations are created in an ordered manner meaning that the product of the previous reaction becomes the substrate for the follow up reaction. This means that the order in which you call these reactions matters! The Rate IDs, i.e. the parameters in the models, are also created in this process. These parameters still have to have a numerical value. This happens in the next function that is called assign_parameter_coefficient.

5) assign_parameter_coefficient(model_components,basal):

The model components contain all the reaction rates pertaining to the network. The basal variable contains the information pertaining to genes with a very low (facultative) basal production rate. The parameters are placed in a dictionary as a key string and assigned a corresponding integer (the c values in the equation for the parameter assignment assigning value between the boundaries). This dictionary is passed to the next function that assigns an actual function. 

6) recombined_parameters(recombined parameters, index_mutation):

The parameters of the previous generations parents are taken and ordered along the nodes that were passed on during the recombination. If a deletion has occurred in the offspring the parameter IDs are corrected for to make sure the correct parameters are associated with the correct node. 

7) reassign_parameter_values(recombined parameters, new parameters)
	
The parameter values of the previous generation take the place of the parameters of the new generation. such that we obtain an updated list of reaction rates which is transformed over the generations.

8) mutate_parameter_value(parameters,index)

The parameters vales are mutated either globally or locally to obtain the final set of reaction equation rates.

9) matrix_construction(model components, parameters, plasmid number)
	
This function performs multiple operations. First it takes the reaction equations and creates a stoichiometric matrix to the interactions between the species and the flux vector according to the equations defined in the paper. If the system is solved stochastically this format is used. for a continuous solver the stoichiometric matrix and the flux vector are unpacked to create a system of ODE's with corresponding empty data vectors that can be filled up by the odeint solver (the only thing in the equations are time vector data points for each species signifying the concentrations at a specific time and the reaction rates as established by the previous functions concerning the parameters). The initial conditions are created in this function and can be adjusted in this function (defined as IC). The plasmid number is an input into this system is multiplies the production rates of the mRNA with the number of plasmids in the system.


10) solver_odeint(states,components,simulation time)

The solver odeint function is a function that builds a function (with the model of the network) that is subsequently solved using the scipy.odeint integrator. If you attempt to do this yourself make sure you compress the evaluation of the strings in order to speed up the integrator. The function solves the model and stores the data in numpy arrays that in turn can be used for scoring the individual with a user defined scoring function.

11) score_model(result, desired Node/protein concentration profile to score against)

this function scores the resulting output of the model. The output contains the concentration profile of all the states in the system. The second input variable into this function finds the concentration profile of the protein the user wishes to score against for an objective. The user has to write the method/s by which this profile is score by his\or herself. there are 2 constraints. The scoring function has to minimise the objective and it has to be stored in a dictionary with the name of the scoring method that is applied, e.g. {'extrema scoring': 1232.43, ' period ' : 15.2}. it is important to test these objective functions, it is often the case that good scores given a pattern data can be attributed to patterns the used does not necessarily desire. The final scores for each individual in the generation come together in the selection function. 

12) store_individual(network,score,data):

This function stores the relevant information of the individual that was created and assessed and 	passes this information to a dictionary containing the information of the other individuals in the generation. If all individuals are present the algorithm can perform a selection, selecting two new parents and restart the process.

13) rank_individuals(generation)

This function ranks the individuals within a generation in terms of fitness according to the method described in the paper. The reason all scoring functions have to minimise this objective is because this function orders the scores - small to large. The names of the scoring methods as described in the dictionaries need to be adjusted in this function (such that the keys match). 

14) select(generation, selection method) 

The select function works if the rank individuals works. there are three selection methods. elitism, semi elitism and rank proportional. This is selection methods is an input in the main file.





	


